var express = require('express'),
    app = express(),
    http = require('http'),
    server = http.createServer(app),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    account = require('./index.js');


mongoose.connect('mongodb://127.0.0.1:27017/rusles_account', function (err) {
    if (err) throw err;

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));
    app.use('/account', account.router);

    server.listen(8081);
    server.on('listening', function () {
        console.log('Express server started on port %s at %s', server.address().port, server.address());
    });
});