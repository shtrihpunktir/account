var mongoose = require('mongoose');
require('mongoose-currency').loadType(mongoose);
var Currency = mongoose.Types.Currency;
var ObjectId = mongoose.Schema.Types.ObjectId;

var schema = mongoose.Schema({
    account_id: {type: ObjectId, ref: 'Account'},
    sum: {type: Number, required: true, min: 1},
    status: {type: String, default: "new", enum: ["new", "accepted", "declined", "closed"]},
    created_at: {type: Date, default: Date.now},
    accepted_at: {type: Date, default: null},
    declined_at: {type: Date, default: null},
    closed_at: {type: Date, default: null},
    comment: {type: String}
}, {collection: 'cash_request'});

module.exports = mongoose.model('CashRequest', schema);