var mongoose = require('mongoose');
require('mongoose-currency').loadType(mongoose);
var Currency = mongoose.Types.Currency;

var schema = mongoose.Schema({
    // _id: {type: mongoose.Types.ObjectId},
    balance: {type: Number},
    operations: [{
        created: {type: Date, default: Date.now},
        income: {type: Boolean, required: true},
        sum: {type: Number},
        comment: {type: String}
    }]
}, {collection: 'account'});

module.exports = mongoose.model('Account', schema);