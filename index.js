"use strict";

var express = require('express')
    , router = express.Router()
    , Account = require('./models/account')
    , CashRequest = require('./models/cash-request')
    , mongoose = require('mongoose');

router.get('/:id/history', function (req, res, next) {
    var accountId = req.params.id;
    return AccountModule.getHistory(accountId).then(function (history) {
        return res.json(history);
    });
});

router.get('/:id/balance', function (req, res, next) {
    var accountId = req.params.id;
    return AccountModule.getBalance(accountId).then(function (balance) {
        return res.json(balance);
    });
});

router.get('/:id/cash-requests', function (req, res, next) {
    return AccountModule.getCashRequests(req.params.id).then(function (requests) {
        return res.json(requests);
    });
});

router.get('/cash-requests', function(req, res, next){
    return AccountModule.getAllCashRequests().then(function(requests){
        return res.json(requests);
    })
});


// создать заявку на вывод наличности со счёта
router.post('/:id/cash-requests', function (req, res, next) {
    var accountId = req.params.id,
        sum = parseInt(req.body.sum);

    return AccountModule.createCashRequest(accountId, sum).then(
        function (cashRequest) {
            res.json(cashRequest);
        }, function (error) {
            return res.status(400).json(error);
        });
});

// одобрить заявку на вывод наличности со счёта
router.put('/:id/cash-requests/:request_id/accept', function (req, res, next) {
    var accountId = req.params.id,
        cashRequestId = req.params.request_id;

    return AccountModule.acceptCashRequest(cashRequestId).then(
        function (cashRequest) {
            res.json(cashRequest);
        }, function (error) {
            res.status(400).json({message: error.message});
        });
});

// отклонить заявку на вывод наличности со счета
router.put('/:id/cash-requests/:request_id/decline', function (req, res, next) {
    var accountId = req.params.id,
        cashRequestId = req.params.request_id,
        comment = req.body.comment;

    return AccountModule.declineCashRequest(cashRequestId, comment).then(
        function (cashRequest) {
            res.json(cashRequest);
        }, function (error) {
            res.status(400).json({message: error.message});
        });
});

router.put('/:id/cash-requests/:request_id/close', function (req, res, next) {
    var accountId = req.params.id,
        cashRequestId = req.params.request_id;

    return AccountModule.closeCashRequest(cashRequestId).then(
        function (cashRequest) {
            res.json(cashRequest);
        }, function (error) {
            res.status(400).json({message: error.message});
        });
});

function loadAccount(account_id) {
    return Account.findById(account_id).then(function (account) {
        if (!account) {
            return Account.create({
                _id: account_id,
                balance: 0,
                operations: []
            });
        }
        return account;
    });
}

function loadCashRequest(cashRequestId) {
    return CashRequest.findById(cashRequestId).then(function (cashRequest) {
        if (!cashRequest)
            throw new Error('Cash request not found');
        return cashRequest;
    });
}

var AccountModule = {
    add: function (account_id, sum, comment) {
        // начислить сумму на счет
        return loadAccount(account_id).then(function (account) {
            sum = parseInt(sum);
            if (sum <= 0) {
                throw new Error('Sum must be greater than zero');
            }

            account.balance += sum;
            account.operations.push({
                income: true,
                sum: sum,
                comment: comment
            });
            return account.save().then(function (account) {
                return account.toObject();
            });
        });
    },
    subtract: function (account_id, sum, comment) {
        // списать сумму со счета
        return loadAccount(account_id).then(function (account) {
            sum = parseInt(sum);
            if (sum <= 0) {
                throw new Error('Sum must be greater than zero');
            }
            if (account.balance < sum) {
                throw new Error('Sum must be less than balance');
            }

            account.balance -= sum;
            account.operations.push({
                income: false,
                sum: sum,
                comment: comment
            });
            return account.save().then(function (account) {
                return account.toObject();
            });
        });
    },
    getHistory: function (account_id) {
        // получить список операций (списаний и начислений) счета
        return loadAccount(account_id).then(function (account) {
            return account.operations;
        });
    },
    getBalance: function (account_id) {
        // получить состояние (баланс) счета
        return loadAccount(account_id).then(function (account) {
            return account.balance;
        });
    },
    getCashRequests: function (account_id) {
        return loadAccount(account_id).then(function (account) {
            return CashRequest.find({account_id: account._id});
        });
    },
    getAllCashRequests: function () {
        return CashRequest.find({});
    },
    createCashRequest: function (account_id, sum) {
        return loadAccount(account_id).then(function (account) {
            var cashRequest = new CashRequest({
                account_id: account._id,
                sum: sum
            });
            return cashRequest.save().then(function (cashRequest) {
                return cashRequest.toObject();
            }, function (error) {
                return error;
            });
        });
    },
    acceptCashRequest: function (id) {
        return loadCashRequest(id).then(function (cashRequest) {
            if ('new' != cashRequest.status) {
                throw {type: "BadRequest", message: 'Only new cash request can be accepted'};
            }
            return loadAccount(cashRequest.account_id).then(function (account) {
                if (cashRequest.sum > account.balance) {
                    throw {type: "BadRequest", message: 'Cash request must be less or equal to account balance'};
                }

                cashRequest.status = 'accepted';
                cashRequest.accepted_at = Date.now();
                return cashRequest.save().then(function (cashRequest) {
                    return cashRequest.toObject();
                }, function (error) {
                    throw {type: error.name, message: error.message};
                });
            });
        });
    },
    declineCashRequest: function (id, reason) {
        return loadCashRequest(id).then(function (cashRequest) {
            if ('new' != cashRequest.status && 'accepted' != cashRequest.status) {
                throw {type: "BadRequest", message: 'Only new or accepted cash request can be declined'};
            }
            return loadAccount(cashRequest.account_id).then(function (account) {
                cashRequest.status = 'declined';
                cashRequest.comment = reason;
                cashRequest.declined_at = Date.now();
                return cashRequest.save().then(function (cashRequest) {
                    return cashRequest.toObject();
                }, function (error) {
                    throw {type: error.name, message: error.message};
                });
            });
        });
    },
    closeCashRequest: function (id) {
        return loadCashRequest(id).then(function (cashRequest) {
            if ('accepted' != cashRequest.status) {
                throw {type: "BadRequest", message: 'Only accepted cash request can be closed'};
            }
            return AccountModule.subtract(cashRequest.account_id, cashRequest.sum).then(function (account) {
                cashRequest.status = 'closed';
                cashRequest.closed_at = Date.now();
                return cashRequest.save();
            });
        }).then(function (cashRequest) {
            return cashRequest.toObject();
        }, function (error) {
            throw {type: error.name, message: error.message};
        });
    },
    router: router
};

module.exports = AccountModule;
